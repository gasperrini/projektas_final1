---
title: "Modelling"
author: Ignas Gasparavicius and Emilija Vingyte

output:
  html_document:
    keep_md: true
---
Užkrauname reikalingas bibliotekas bei paleidžiame H2O.


```
## Warning: package 'h2o' was built under R version 4.1.2
```

```
## 
## ----------------------------------------------------------------------
## 
## Your next step is to start H2O:
##     > h2o.init()
## 
## For H2O package documentation, ask for help:
##     > ??h2o
## 
## After starting H2O, you can use the Web UI at http://localhost:54321
## For more information visit https://docs.h2o.ai
## 
## ----------------------------------------------------------------------
```

```
## 
## Attaching package: 'h2o'
```

```
## The following objects are masked from 'package:stats':
## 
##     cor, sd, var
```

```
## The following objects are masked from 'package:base':
## 
##     %*%, %in%, &&, ||, apply, as.factor, as.numeric, colnames,
##     colnames<-, ifelse, is.character, is.factor, is.numeric, log,
##     log10, log1p, log2, round, signif, trunc
```

```
##  Connection successful!
## 
## R is connected to the H2O cluster: 
##     H2O cluster uptime:         31 minutes 46 seconds 
##     H2O cluster timezone:       Europe/Vilnius 
##     H2O data parsing timezone:  UTC 
##     H2O cluster version:        3.34.0.3 
##     H2O cluster version age:    2 months and 14 days  
##     H2O cluster name:           H2O_started_from_R_37060_rxd351 
##     H2O cluster total nodes:    1 
##     H2O cluster total memory:   1.78 GB 
##     H2O cluster total cores:    12 
##     H2O cluster allowed cores:  12 
##     H2O cluster healthy:        TRUE 
##     H2O Connection ip:          localhost 
##     H2O Connection port:        54321 
##     H2O Connection proxy:       NA 
##     H2O Internal Security:      FALSE 
##     H2O API Extensions:         Amazon S3, Algos, AutoML, Core V3, TargetEncoder, Core V4 
##     R Version:                  R version 4.1.1 (2021-08-10)
```

Modelio paieškai buvo pasitelktas AutoML su parametru:
* max_runtime_secs = 14400

AutoML geriausias rezultatas - GBM modelis (AUC > 0.82). Būtent šis modelis ir buvo pasirinktas tolimesniam optimizavimui. 

Išanalizabus AutoML sukurto modelio parametrus, pasirinkta, kuriuos iš jų verta optimizuoti. h2o.gbm funkcija paleista keletą kartų manipuliuojant modelio parametrais.

Geriausias modelis išsaugotas **4-model/GBM_model**. Jo hyperparametrai nurodomi žemiau.

Nuskaitomas pilnas duomenų failas ir išvedama jo dimensijos.

```
## $model_id
## [1] "GBM_model_R_1639640409622_1584"
## 
## $nfolds
## [1] 5
## 
## $keep_cross_validation_models
## [1] FALSE
## 
## $keep_cross_validation_predictions
## [1] TRUE
## 
## $keep_cross_validation_fold_assignment
## [1] FALSE
## 
## $score_each_iteration
## [1] FALSE
## 
## $score_tree_interval
## [1] 5
## 
## $fold_assignment
## [1] "Modulo"
## 
## $ignore_const_cols
## [1] TRUE
## 
## $balance_classes
## [1] FALSE
## 
## $max_after_balance_size
## [1] 5
## 
## $max_confusion_matrix_size
## [1] 20
## 
## $ntrees
## [1] 45
## 
## $max_depth
## [1] 15
## 
## $min_rows
## [1] 100
## 
## $nbins
## [1] 20
## 
## $nbins_top_level
## [1] 1024
## 
## $nbins_cats
## [1] 1024
## 
## $r2_stopping
## [1] 1.797693e+308
## 
## $stopping_rounds
## [1] 0
## 
## $stopping_metric
## [1] "logloss"
## 
## $stopping_tolerance
## [1] 0.001
## 
## $max_runtime_secs
## [1] 0
## 
## $seed
## [1] 1234
## 
## $build_tree_one_node
## [1] FALSE
## 
## $learn_rate
## [1] 0.1
## 
## $learn_rate_annealing
## [1] 1
## 
## $distribution
## [1] "bernoulli"
## 
## $quantile_alpha
## [1] 0.5
## 
## $tweedie_power
## [1] 1.5
## 
## $huber_alpha
## [1] 0.9
## 
## $sample_rate
## [1] 1
## 
## $col_sample_rate
## [1] 1
## 
## $col_sample_rate_change_per_level
## [1] 1
## 
## $col_sample_rate_per_tree
## [1] 1
## 
## $min_split_improvement
## [1] 1e-05
## 
## $histogram_type
## [1] "UniformAdaptive"
## 
## $max_abs_leafnode_pred
## [1] 1.797693e+308
## 
## $pred_noise_bandwidth
## [1] 0
## 
## $categorical_encoding
## [1] "Enum"
## 
## $calibrate_model
## [1] FALSE
## 
## $check_constant_response
## [1] TRUE
## 
## $gainslift_bins
## [1] -1
## 
## $auc_type
## [1] "AUTO"
## 
## $x
##  [1] "amount_current_loan"          "term"                        
##  [3] "credit_score"                 "loan_purpose"                
##  [5] "yearly_income"                "home_ownership"              
##  [7] "bankruptcies"                 "years_current_job"           
##  [9] "monthly_debt"                 "years_credit_history"        
## [11] "months_since_last_delinquent" "open_accounts"               
## [13] "credit_problems"              "credit_balance"              
## [15] "max_open_credit"             
## 
## $y
## [1] "y"
```


**Gauta AUC metrika**:

* training = 0.8459
* validation = 0.8314
* test = 0.8245

Treniravimo kodas išsaugotas **3-R/modelling_customised**. 

Modifikuoti parametrai:

* nfolds pasirinktas 5;
* ntrees pakelta iki 45;
* max_depth pasirinktas 15;
* optimalus learn_rate pasirinktas 0,1;
* likę parametrai palikti pagal AutoML geriausio modelio parametrus.

Geriausiam modeliui sukurta Shiny web aplikacija, kuri išsaugota **app** folderyje.


