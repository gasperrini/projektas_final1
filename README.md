# Nuoroda į analitinę web aplikaciją

https://gasperrini.shinyapps.io/dvda2021_test/

# Užduotis

Sukurkite duomenų produktą - analitinę aplikaciją, skirtą banko paskolos įvertinimui mašininio mokymosi algoritmų pagalba

# Duomenys

Paskolų įsipareigojimo nevykdymas (loan default)

* id - unikalus ID 
* y - ar įvykdytas paskolos įsipareigojimas (0 - TAIP, 1 - NE)
* kiti kintamieji - paskolos parametrai (pvz. kredito istorija, paskolos tipas)

Duomenis galite atsisiųsti iš:

[MS OneDrive](https://ktuedu-my.sharepoint.com/:f:/g/personal/kesdau_ktu_lt/EkbHuxm1KhBLvD7GXyrBTz4BY-OtB2d64EWCPCJ1NdmDww?e=go9C6u)

arba

[Google Drive](https://drive.google.com/drive/folders/17NsP84MecXHyctM94NLwps_tsowld_y8?usp=sharing)
