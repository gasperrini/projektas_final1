---
title: "Exploratory Analysis"
author: Ignas Gasparavicius and Emilija Vingyte

output:
  html_document:
    keep_md: true
---
Užkrauname reikalingas bibliotekas.


```
## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --
```

```
## v ggplot2 3.3.5     v purrr   0.3.4
## v tibble  3.1.5     v dplyr   1.0.7
## v tidyr   1.1.4     v stringr 1.4.0
## v readr   2.0.2     v forcats 0.5.1
```

```
## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```


Nuskaitomas pilnas duomenų failas ir išvedama jo dimensijos.

```
## Rows: 10000000 Columns: 17
```

```
## -- Column specification --------------------------------------------------------
## Delimiter: ","
## chr  (4): term, credit_score, loan_purpose, home_ownership
## dbl (13): id, y, amount_current_loan, yearly_income, bankruptcies, years_cur...
```

```
## 
## i Use `spec()` to retrieve the full column specification for this data.
## i Specify the column types or set `show_col_types = FALSE` to quiet this message.
```

```
## [1] 10000000       17
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

# Kintamųjų apžvalga

Duomenų failas sudarytas iš 17 kintamųjų - 4 kokybinių ir likusieji - kiekybiniai. Pirmieji du stulpeliai - "id" ir "y" nurodo eilutės identifikacinį numerį bei klasifikuoja paskolas į vedančias prie bankroto arba ne (atitinkamai 1 arba 0).

|   |      id        |      y        |amount_current_loan |    term         |credit_score     |loan_purpose     |
|:--|:---------------|:--------------|:-------------------|:----------------|:----------------|:----------------|
|   |Min.   :1.0e+00 |Min.   :0.0000 |Min.   :   10802    |Length:10000000  |Length:10000000  |Length:10000000  |
|   |1st Qu.:2.5e+06 |1st Qu.:0.0000 |1st Qu.:  179652    |Class :character |Class :character |Class :character |
|   |Median :5.0e+06 |Median :0.0000 |Median :  312312    |Mode  :character |Mode  :character |Mode  :character |
|   |Mean   :5.0e+06 |Mean   :0.2421 |Mean   :11765909    |NA               |NA               |NA               |
|   |3rd Qu.:7.5e+06 |3rd Qu.:0.0000 |3rd Qu.:  525096    |NA               |NA               |NA               |
|   |Max.   :1.0e+07 |Max.   :1.0000 |Max.   :99999999    |NA               |NA               |NA               |


|   |yearly_income     |home_ownership   | bankruptcies |years_current_job | monthly_debt  |years_credit_history |months_since_last_delinquent |
|:--|:-----------------|:----------------|:-------------|:-----------------|:--------------|:--------------------|:----------------------------|
|   |Min.   :    76627 |Length:10000000  |Min.   :0.000 |Min.   : 0.0      |Min.   :     0 |Min.   : 4.00        |Min.   :  0                  |
|   |1st Qu.:   848844 |Class :character |1st Qu.:0.000 |1st Qu.: 3.0      |1st Qu.: 10200 |1st Qu.:14.00        |1st Qu.: 16                  |
|   |Median :  1174371 |Mode  :character |Median :0.000 |Median : 6.0      |Median : 16221 |Median :17.00        |Median : 32                  |
|   |Mean   :  1378367 |NA               |Mean   :0.117 |Mean   : 5.9      |Mean   : 18471 |Mean   :18.22        |Mean   : 35                  |
|   |3rd Qu.:  1651499 |NA               |3rd Qu.:0.000 |3rd Qu.:10.0      |3rd Qu.: 24012 |3rd Qu.:22.00        |3rd Qu.: 51                  |
|   |Max.   :165557393 |NA               |Max.   :7.000 |Max.   :10.0      |Max.   :435843 |Max.   :70.00        |Max.   :176                  |
|   |NA's   :1919747   |NA               |NA's   :18354 |NA's   :422327    |NA             |NA                   |NA's   :5317819              |


|   |open_accounts |credit_problems |credit_balance   |max_open_credit   |
|:--|:-------------|:---------------|:----------------|:-----------------|
|   |Min.   : 0.00 |Min.   : 0.0000 |Min.   :       0 |Min.   :0.000e+00 |
|   |1st Qu.: 8.00 |1st Qu.: 0.0000 |1st Qu.:  113297 |1st Qu.:2.771e+05 |
|   |Median :10.00 |Median : 0.0000 |Median :  210767 |Median :4.726e+05 |
|   |Mean   :11.15 |Mean   : 0.1736 |Mean   :  297283 |Mean   :7.687e+05 |
|   |3rd Qu.:14.00 |3rd Qu.: 0.0000 |3rd Qu.:  370253 |3rd Qu.:7.914e+05 |
|   |Max.   :76.00 |Max.   :15.0000 |Max.   :32878968 |Max.   :1.540e+09 |
|   |NA            |NA              |NA               |NA's   :192       |


|                     |       x|
|:--------------------|-------:|
|business_loan        |  155856|
|buy_a_car            |  125140|
|buy_house            |   67762|
|debt_consolidation   | 7868889|
|educational_expenses |   10160|
|home_improvements    |  584493|
|major_purchase       |   35029|
|medical_bills        |  111058|
|moving               |   14991|
|other                |  920066|
|renewable_energy     |     985|
|small_business       |   27701|
|take_a_trip          |   55982|
|UFVCU1VC             |       1|
|vacation             |   10039|
|wedding              |   11848|

Tolimesnei apžvalgai pasirinkta keletas reikšmingesnių kintamųjų - years_current_job (kiek metų paskolą imantis klientas praleido dabartiniame darbe), term (ilgalaikė ar trumpalaikė paskola), credit_score(kredito vertinimas labai geras, geras, prastas ar nėra duomenų) bei loan_purpose (paskolos tikslas).
Galima pastebėti, jog paskolų, vedančių į bankrotą santykis su sėkmingomis paskolomis beveik nesiskiria tarp klientų, dabartinėje darbovietėje praleidusių skirtingą laiko tarpą. Išankstinė nuomonė apie kintamąjį galimai klaidinga ir jis vis dėlto ne itin reikšmingas šiam klasifikavimo uždaviniui.

![](report_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

Tuo tarpu paskolos trukmės grafikas atskleidžia, jog trumpalaikės paskolos santykinai yra daug sėkmingesnės nei ilgalaikės.

![](report_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

Labai gerą kredito vertinimą turinčius klientus paskola itin retai nuveda prie bankroto. Dažnesni bankroto atvejai - gerą kredito vertinimą turintiems klientams, o prastą kredito vertinimą turintys klientai bankrotą patiria beveik 50% iš tiriamų atvejų. Duomenų apie kredito vertinimą neturinčių klientų bankroto atvejai dažniausi - daugiau nei 2/3.

![](report_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

Dažniausia paskolos priežastis - siekis padengti kitas turimas paskolas. Būtent tokie atvejai dažniausiai ir veda į bankrotą. Dar viena dažna paskolos priežastis - namų atnaujinimai/ patobulinimai, tarp kurių bankroto atvejų taip pat nemažai.

![](report_files/figure-html/unnamed-chunk-10-1.png)<!-- -->



