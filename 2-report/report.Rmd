---
title: "Exploratory Analysis"
author: Ignas Gasparavicius and Emilija Vingyte

output:
  html_document:
    keep_md: true
---
Užkrauname reikalingas bibliotekas.

```{r, echo=FALSE}
library(tidyverse)
library(knitr)
```


Nuskaitomas pilnas duomenų failas ir išvedama jo dimensijos.
```{r, echo=FALSE, cache=TRUE}
df <- read_csv("data_full.csv")
dim(df)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

# Kintamųjų apžvalga

Duomenų failas sudarytas iš 17 kintamųjų - 4 kokybinių ir likusieji - kiekybiniai. Pirmieji du stulpeliai - "id" ir "y" nurodo eilutės identifikacinį numerį bei klasifikuoja paskolas į vedančias prie bankroto arba ne (atitinkamai 1 arba 0).
```{r, echo=FALSE, message=FALSE, warning=FALSE}
summary(df[1:6]) %>%
  kable()
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
summary(df[7:13]) %>%
  kable()
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
summary(df[14:17]) %>%
  kable()
```

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df$loan_purpose <-as.factor(df$loan_purpose)
df$y <- as.factor(df$y)
summary(df$loan_purpose) %>%
  kable()
```

Tolimesnei apžvalgai pasirinkta keletas reikšmingesnių kintamųjų - years_current_job (kiek metų paskolą imantis klientas praleido dabartiniame darbe), term (ilgalaikė ar trumpalaikė paskola), credit_score(kredito vertinimas labai geras, geras, prastas ar nėra duomenų) bei loan_purpose (paskolos tikslas).
Galima pastebėti, jog paskolų, vedančių į bankrotą santykis su sėkmingomis paskolomis beveik nesiskiria tarp klientų, dabartinėje darbovietėje praleidusių skirtingą laiko tarpą. Išankstinė nuomonė apie kintamąjį galimai klaidinga ir jis vis dėlto ne itin reikšmingas šiam klasifikavimo uždaviniui.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df %>%
  group_by(y, years_current_job) %>%
  summarise(n = n()) %>%
  ggplot(aes(fill=y, y=n, x=years_current_job)) + 
    geom_bar(position="dodge", stat="identity") + 
    coord_flip() +
    scale_y_continuous(labels = scales::comma) +
    theme_dark()
```

Tuo tarpu paskolos trukmės grafikas atskleidžia, jog trumpalaikės paskolos santykinai yra daug sėkmingesnės nei ilgalaikės.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df %>%
  group_by(y, term) %>%
  summarise(n = n()) %>%
  ggplot(aes(fill=y, y=n, x=term)) + 
    geom_bar(position="dodge", stat="identity") + 
    coord_flip() +
    scale_y_continuous(labels = scales::comma) +
    theme_dark()
```

Labai gerą kredito vertinimą turinčius klientus paskola itin retai nuveda prie bankroto. Dažnesni bankroto atvejai - gerą kredito vertinimą turintiems klientams, o prastą kredito vertinimą turintys klientai bankrotą patiria beveik 50% iš tiriamų atvejų. Duomenų apie kredito vertinimą neturinčių klientų bankroto atvejai dažniausi - daugiau nei 2/3.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df %>%
  group_by(y, credit_score) %>%
  summarise(n = n()) %>%
  ggplot(aes(fill=y, y=n, x=credit_score)) + 
    geom_bar(position="dodge", stat="identity") + 
    coord_flip() +
    scale_y_continuous(labels = scales::comma) +
    theme_dark()
```

Dažniausia paskolos priežastis - siekis padengti kitas turimas paskolas. Būtent tokie atvejai dažniausiai ir veda į bankrotą. Dar viena dažna paskolos priežastis - namų atnaujinimai/ patobulinimai, tarp kurių bankroto atvejų taip pat nemažai.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df %>%
  group_by(y, loan_purpose) %>%
  summarise(n = n()) %>%
  ggplot(aes(fill=y, y=n, x=loan_purpose)) + 
    geom_bar(position="dodge", stat="identity") + 
    coord_flip() +
    scale_y_continuous(labels = scales::comma) +
    theme_dark()
```



